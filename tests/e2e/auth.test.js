/**
 * Testing post user endpoint
 */

import request from 'supertest'
import server from '../../server'

// const TIMEOUT_INTERVAL = 30000;
// var http = require('http');
const req = request(server)

describe('POST /api/auth/register', () => {
  test(
    'successful registration',
    async (done) => {
      const payload = {
        fullname: 'Testing100',
        handle: 'testing100',
        email: 'testing100@gmail.com',
        emailRetype: 'testing100@gmail.com',
        password: 'testing100'
      }
      const response = await req.post('/api/auth/register').send(payload)
      expect(response.status).toEqual(200)
      done()
    }
  )
  it(
    'user already exists',
    async done => {
      expect.assertions(5)
      const payload = {
        fullname: 'Testing100',
        handle: 'testing100',
        email: 'testing100@gmail.com',
        emailRetype: 'testing100@gmail.com',
        password: 'testing100'
      }
      const UserData = await req.post('/api/auth/register').send(payload)

      // console.log(UserData.body)
      expect(UserData.statusCode).toBe(422)
      expect(UserData.body).toBeDefined()
      expect(UserData.body.success).toEqual(false)
      expect(UserData.body.statusMessage).not.toBe('OK')
      expect(UserData.body.success).toEqual(false)

      done()
    },
    10000
  )

  it(
    'Emails do not match',
    async done => {
      expect.assertions(2)
      const payload = {
        fullname: 'Testing',
        handle: 'testing',
        email: 'testing100@gmail.com',
        emailRetype: 'testing@gmail.com',
        password: 'testing'
      }
      const UserData = await req.post('/api/auth/register').send(payload)

      // console.log(UserData)
      expect(UserData.body.success).toEqual(false)
      expect(UserData.body.errors).toContain('Emails do not match')

      done()
    },
    10000
  )

  it(
    'Registration field validation',
    async done => {
      expect.assertions(3)
      const payload = {
        fullname: 'Testing',
        handle: 'te',
        email: 'testing100gmail.com',
        emailRetype: 'testing@gmail.com',
        password: 'testing'
      }
      const UserData = await req.post('/api/auth/register').send(payload)

      // console.log(UserData)
      expect(UserData.body.success).toEqual(false)
      expect(UserData.body.errors).toContain('Please provide a valid email address')
      expect(UserData.body.errors).toContain('Handle should have at least 3 characters')

      done()
    },
    10000
  )

  it(
    'Empty field validation',
    async done => {
      expect.assertions(3)
      const payload = {
        fullname: 'Testing',
        password: 'testing'
      }
      const UserData = await req.post('/api/auth/register').send(payload)

      // console.log(UserData)
      expect(UserData.body.success).toEqual(false)
      expect(UserData.body.errors).toContain('Handle field is required')
      expect(UserData.body.errors).toContain('Email field is required')

      done()
    },
    10000
  )
})
