import BaseModel from './base'
import ProblemDescriptionHistory from './problemDescriptionHistory'

const TABLE_NAME = 'problems'

/**
 * Problem model.
 */
class Problem extends BaseModel {
  static get tableName () {
    return TABLE_NAME
  }
  static get idColumn () {
    return 'problemId'
  }

  static get relationMappings () {
    return {
      problemDescriptionHistory: {
        relation: BaseModel.HasManyRelation,
        modelClass: ProblemDescriptionHistory,
        join: {
          from: 'problems.problemId',
          to: 'problemDescriptionHistory.problemId'
        }
      }
    }
  }
}

export default Problem
