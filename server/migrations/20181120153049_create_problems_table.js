exports.up = function (knex, Promise) {
  return knex.schema.createTable('problems', function (t) {
    t.bigIncrements('problemId').unsigned().primary()
    t.string('problemHandleStr', 30).unique().notNullable()
    t.string('problemTitleStr').notNullable()
    t.integer('codeLimitKbInt').notNullable().defaultTo(32)
    t.enu('visibilityStr', ['public', 'private']).notNullable().defaultTo('private')
    t.enu('problemTypeStr', ['open', 'default']).notNullable().defaultTo('default')
    t.enu('judgeTypeStr', ['float', 'default', 'special']).notNullable().defaultTo('default')
    t.string('floatJudgeFormatStr', 255).nullable()
    t.integer('floatJudgeNumDigitsInt').defaultTo(6)
    t.enu('problemStateStr', ['in-progress', 'ready-for-testing', 'done']).notNullable().defaultTo('in-progress')
    t.boolean('isScoreSupportedBool').defaultTo(0)
    t.boolean('isReadyBool').defaultTo(false)
    t.boolean('isLojBool').notNullable().defaultTo(false)
    t.integer('numPendingDescInt').defaultTo(0)
    // foreign key
    t.bigInteger('userId').unsigned().notNullable().references('users.userId')
    // utils
    t.timestamps() // will create created_at, updated_at
    t.timestamp('deleted_at').nullable()
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTableIfExists('problems')
}
