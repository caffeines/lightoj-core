exports.up = function (knex, Promise) {
  return knex.schema.createTable('social_auth', t => {
    t.bigIncrements('socialAuthId').unsigned().primary()
    t.bigInteger('userId').unsigned().notNullable()
    t.string('provider')
    t.string('accountId')
    t.string('profileLink')
    t.string('accessToken')
    t.string('refreshToken')
    t.timestamps()
    t.timestamp('deleted_at').nullable()
    t.foreign('userId').references('users.userId')
    t.unique(['provider', 'accountId'])
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTableIfExists('social_auth')
}
