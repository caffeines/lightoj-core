exports.up = async function (knex) {
  await knex.schema.createTable('problemPermissions', function (t) {
    t.bigIncrements('problemPermissionId').unsigned().primary()
    t.bigInteger('problemId').unsigned().notNullable().references('problems.problemId')
    t.bigInteger('userId').unsigned().notNullable().references('users.userId')
    t.enu('roleTypeStr', ['owner', 'editor', 'tester', 'viewer']).defaultTo('viewer')
    t.timestamp('deleted_at').nullable()
    t.timestamps()
  })
}

exports.down = async function (knex) {
  await knex.schema.dropTableIfExists('problemPermissions')
}
