import express from 'express'
import { Nuxt, Builder } from 'nuxt'
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import morgan from 'morgan'
import compression from 'compression'
import jwt from 'express-jwt'
import config from 'config'
import passport from 'passport'
import routes from './routes'
import fileUpload from 'express-fileupload'

const app = express()

app.use(morgan('tiny'))
app.use(bodyParser.json())
app.use(cookieParser())
app.use(compression())
app.use(passport.initialize())
app.use(fileUpload({
  limits: { fileSize: config.fileUpload.max_upload_size * 1024 * 1024 },
  parseNested: true
}))

// app.set('port', port)

// JWT middleware
const jwtMiddleware = jwt({
  secret: config.jwt.secret,
  credentialsRequired: false,
  getToken: function (req) {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') { // Authorization: Bearer g1jipjgi1ifjioj
      // Handle token presented as a Bearer token in the Authorization header
      return req.headers.authorization.split(' ')[1]
    } else if (req.query && req.query.token) {
      // Handle token presented as URI param
      return req.query.token
    } else if (req.cookies && req.cookies.token) {
      // Handle token presented as a cookie parameter
      return req.cookies.token
    }
    // If we return null, we couldn't find a token.
    // In this case, the JWT middleware will return a 401 (unauthorized) to the client for this request
    return null
  }
})

app.use(jwtMiddleware)

// Import API Routes
app.use('/api', routes)

// Import and Set Nuxt.js options
let nuxtConfig = require('../nuxt.config.js')
nuxtConfig.dev = !(process.env.NODE_ENV === 'production')

// Init Nuxt.js
const nuxt = new Nuxt(nuxtConfig)

// Build only in dev mode
if (nuxtConfig.dev) {
  const builder = new Builder(nuxt)
  builder.build()
}

// Give nuxt middleware to express
app.use(nuxt.render)

function errorHandler (err, req, res, next) {
  if (err) {
    console.log(err)
  }
  return res.send(500).json({'errors': ['Something went wrong!']})
}
passport.serializeUser(function (user, done) {
  done(null, user)
})

passport.deserializeUser(function (user, done) {
  done(null, user)
})

app.use(function (req, res, next) {
  res.status(404).json({'errors': ['Route not found']})
})

app.use(errorHandler)

module.exports = app
