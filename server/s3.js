import fs from 'fs'
import config from 'config'
import AWS from 'aws-sdk'

const s3Config = {
  s3ForcePathStyle: config.s3.s3ForcePathStyle,
  accessKeyId: config.s3.accessKeyId,
  secretAccessKey: config.s3.secretAccessKey,
  endpoint: new AWS.Endpoint(config.s3.endpoint)
}

const client = new AWS.S3(s3Config)

export function uploadFile (filePath, Key, Bucket) {
  const params = {
    Key,
    Bucket,
    Body: fs.createReadStream(filePath)
  }

  return new Promise((resolve, reject) => {
    client.upload(params, function uploadCallback (err, data) {
      if (err) {
        reject(err)
      }
      resolve(data)
    })
  })
}

export function createBucket (Bucket) {
  var params = {
    Bucket
  }
  return client.createBucket(params).promise()
}

export function uploadStream (file, Key, Bucket) {
  const params = {
    Body: file.data,
    ContentType: file.mimetype,
    ContentLength: file.length,
    Key,
    Bucket
  }
  return client.putObject(params).promise()
}
