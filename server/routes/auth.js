import {Router} from 'express'
import {check, validationResult} from 'express-validator/check'

import * as userService from '../services/userService'
import {jwtSign} from '../utils'
import {errorResponse, successResponse} from '../utils/response'

const router = Router()

router.post(
  '/register',
  [
    check('email')
      .exists()
      .withMessage('Email field is required')
      .isLength({min: 3})
      .withMessage('Email should have at least 3 characters')
      .isEmail()
      .withMessage('Please provide a valid email address')
      .custom(async (value, {req, loc, path}) => {
        const exist = await userService.checkEmailExist(value)
        return (exist === true) ? false : value
      })
      .withMessage('Email already exists')
      .custom((value, {req}) => {
        return value.toLowerCase() === req.body.emailRetype.toLowerCase()
      })
      .withMessage('Emails do not match'),
    check('handle')
      .exists()
      .withMessage('Handle field is required')
      .isLength({min: 3})
      .withMessage('Handle should have at least 3 characters')
      .custom(async (value, {req, loc, path}) => {
        const exist = await userService.checkHandleExist(value)
        return (exist === true) ? false : value
      }).withMessage('Handle already exists')
  ],
  async function (req, res, next) {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return errorResponse(res, errors.array().map(error => error.msg))
    }
    const user = await userService.createUser(req.body)
    if (!user) {
      return errorResponse(res, 'Something went wrong!')
    }
    return successResponse(res)
  })

router.post('/login', [
  check('handleOrEmail').exists().withMessage('Please provide your handle or email'),
  check('password').exists().withMessage('Please provide your password')
], async (req, res, next) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return errorResponse(res, errors.array().map(error => error.msg))
  }
  const user = await userService.loginUserWithHandleOrEmail(req.body.handleOrEmail, req.body.password)
  if (!user) {
    return errorResponse(res, 'User and password combination didn\'t match')
  }
  const userData = {
    fullName: user.userNameStr,
    handle: user.userHandleStr,
    email: user.userEmailStr,
    id: user.userId
  }
  const returnedData = {
    userData,
    token: jwtSign(userData)
  }
  return successResponse(res, 'Successfully logged in', returnedData)
})

export default router
