import { Router } from 'express'
import * as passportStrategyService from '../services/passportStrategyService'

const passport = require('passport')

const router = Router()

//  Use the GoogleStrategy within Passport.
//  Strategies in Passport require a `verify` function, which accept
//  credentials (in this case, an accessToken, refreshToken, and Google
//  profile), and invoke a callback with a user object.
passport.use(passportStrategyService.getGoogleStrategy())
passport.use(passportStrategyService.getFacebookStrategy())

//  GET /auth/google
//  Use passport.authenticate() as route middleware to authenticate the
//  request.  The first step in Google authentication will involve
//  redirecting the user to google.com.  After authorization, Google
//  will redirect the user back to this application at /auth/google/callback
router.get('/google', passport.authenticate('google', { scope: ['email'] }))
router.get('/facebook', passport.authenticate('facebook', { scope: ['email', 'user_link'] }))

//  GET /auth/google/callback
//  Use passport.authenticate() as route middleware to authenticate the
//  request.  If authentication fails, the user will be redirected back to the
//  login page.  Otherwise, the primary route function function will be called,
//  which, in this example, will redirect the user to the home page.

router.get(
  '/google/callback',
  passport.authenticate('google', { failureRedirect: '/login' }),
  function (req, res) {
    res.cookie('token', req.user.token)
    res.redirect('/')
  }
)

router.get(
  '/facebook/callback',
  passport.authenticate('facebook', { failureRedirect: '/login' }),
  function (req, res) {
    res.cookie('token', req.user.token)
    res.redirect('/')
  }
)

export default router
