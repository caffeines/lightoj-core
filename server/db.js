import knexJs from 'knex'
import knexConfig from '../knexfile'
import objection from 'objection'
import objectionSoftDelete from 'objection-softdelete'

/**
 * Database connection.
 */
const knex = knexJs(knexConfig)
objection.Model.knex(knex)

objectionSoftDelete.register(objection, {
  deleteAttr: 'deleted_at'
})
export default objection
