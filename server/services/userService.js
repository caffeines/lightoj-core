import bcrypt from 'bcrypt'

import User from '../models/user'

const getHashedPassword = async (password) => {
  const salt = await bcrypt.genSalt(10)
  const hash = await bcrypt.hash(password, salt)
  return hash
}

const matchPasswordWithHash = async (givenPassword, hashedPassword) => {
  const matches = await bcrypt.compare(givenPassword, hashedPassword)
  if (matches) return true
  return false
}

export async function createUser (userData) {
  const createData = {
    userNameStr: userData.fullname,
    userHandleStr: userData.handle,
    userEmailStr: userData.email,
    userPassStr: await getHashedPassword(userData.password)
  }
  try {
    const user = await User.query().insert(createData)
    return user
  } catch (err) {
    console.log(err)
    return null
  }
}

export async function checkEmailExist (email) {
  const res = await User.query().where('userEmailStr', email)
  return (res.length > 0)
}

export async function checkHandleExist (handle) {
  const res = await User.query().where('userHandleStr', handle)
  return (res.length > 0)
}

/**
 * Checks two things:
 *   1) if user is found for the handle/email and
 *   2) if the given password is same.
 */
export async function loginUserWithHandleOrEmail (handleOrEmail, password) {
  const user = await User.query()
    .where('userHandleStr', handleOrEmail)
    .orWhere('userEmailStr', handleOrEmail).first()
  if (!user) { return null } // No user found with the given handle
  const match = await matchPasswordWithHash(password, user.userPassStr)
  return (match === true) ? user : null
}

export async function getUserByEmail (email) {
  const user = await User.query().where('userEmailStr', email).first()
  return user
}

export async function get (userId) {
  const user = await User.query().where('userId', userId).first()
  return user
}

export async function getOrCreate (data) {
  let user = await getUserByEmail(data.email)
  if (!user) {
    user = await createUser(data)
  }
  return user
}
