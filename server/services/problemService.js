import Problem from '../models/problem'

export async function checkHandleExist (problemHandle) {
  const res = await Problem.query().where('problemHandleStr', problemHandle)
  return (res.length > 0)
}

export async function getProblem (problemHandle) {
  const res = await Problem.query().where('problemHandleStr', problemHandle).first()
  return res
}

export async function getProblemIdByHandle (problemHandle) {
  const res = await Problem.query().where({'problemHandleStr': problemHandle}).first()
  return res.problemId
}
