import * as socialAuthService from './socialAuthService'
import {jwtSign} from '../utils'
const {socialAuthConfig} = require('config')
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy
var FacebookStrategy = require('passport-facebook').Strategy

async function callback (accessToken, refreshToken, profile, done) {
  try {
    let user = await socialAuthService.getUser(accessToken, refreshToken, profile)
    let userData = {
      fullName: user.userNameStr,
      handle: user.userHandleStr,
      email: user.userEmailStr
    }
    let returnedData = {
      'msg': 'Successfully logged in',
      userData,
      token: jwtSign(userData)
    }
    done(null, returnedData)
  } catch (error) {
    console.log(error)
    done(error, false, error.message)
  }
}

export function getGoogleStrategy () {
  return new GoogleStrategy(socialAuthConfig.Google, callback)
}

export function getFacebookStrategy () {
  return new FacebookStrategy(socialAuthConfig.Facebook, callback)
}
