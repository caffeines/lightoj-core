export const state = () => ({
  locales: ['en', 'bn'],
  locale: 'en',
  loading: false,
  errors: []
})

export const mutations = {
  SET_LANG (state, locale) {
    if (state.locales.indexOf(locale) !== -1) {
      state.locale = locale
    }
  },
  SET_ERRORS (state, errors) {
    state.errors = errors
  },
  CLEAR_ERRORS (state) {
    state.errors = []
  },
  SHOW_LOADING_SPINNER (state) {
    state.loading = true
  },
  HIDE_LOADING_SPINNER (state) {
    state.loading = false
  },
  TOGGLE_LOADING_SPINNER (state) {
    state.loading = !state.loading
  }
}

export const actions = {
  nuxtServerInit ({ commit }, { req }) {
    if (req.user) {
      commit('auth/INIT_LOGIN', req.user)
    }
  },
  setErrors ({ commit }, errors) {
    commit('SET_ERRORS', errors)
  },
  clearErrors ({commit}) {
    commit('CLEAR_ERRORS')
  },
  showLoader ({ commit }) {
    commit('SHOW_LOADING_SPINNER')
  },
  hideLoader ({ commit }) {
    commit('HIDE_LOADING_SPINNER')
  },
  toggleLoader ({commit}) {
    commit('TOGGLE_LOADING_SPINNER')
  }
}
