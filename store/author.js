export const actions = {
  async createProblem ({commit}, problem) {
    try {
      const resp = await this.$axios.$post('author/problem', {
        problemTitle: problem.title,
        problemHandle: problem.handle
      })
      return resp
    } catch (e) {
      let returnData = {
        'success': false,
        'errors': []
      }
      if (e.response.status === 422) {
        /* its a validation error */
        returnData.errors = e.response.data.errors
      } else {
        returnData.errors = ['Something weird! Please try again.']
      }
      return returnData
    }
  },
  async getProblems ({commit}) {
    const resp = await this.$axios.$get('author/problems')
    return resp
  },
  async getProblemByHandle ({commit}, problemHandle) {
    try {
      const resp = await this.$axios.$get(`author/problem/${problemHandle}`)
      return resp
    } catch (e) {
      const returnData = {
        statusCode: e.response.status,
        success: false,
        errors: []
      }
      return returnData
    }
  },
  async updateProblem ({commit}, problemData) {
    const problemHandle = problemData.problemHandleStr
    try {
      const resp = await this.$axios.$post(`author/problem/${problemHandle}`, {
        problemData
      })
      return resp
    } catch (e) {
      const returnData = {
        statusCode: e.response.status,
        success: false,
        errors: []
      }
      return returnData
    }
  },
  async addDataset ({commit}, {problemHandle, datasetData}) {
    let data = new FormData()

    for (let k in datasetData) {
      data.set(k, datasetData[k])
    }

    try {
      const resp = await this.$axios.$post(`author/problem/${problemHandle}/dataset`, data)
      return resp
    } catch (e) {
      let returnData = {
        'success': false,
        'errors': []
      }
      if (e.response.status === 422) {
        /* its a validation error */
        returnData.errors = e.response.data.errors
      } else {
        returnData.errors = ['Something weird! Please try again.']
      }
      return returnData
    }
  },
  async getDatasets ({commit}, problemHandle) {
    try {
      const resp = await this.$axios.$get(`author/problem/${problemHandle}/dataset`)
      return resp
    } catch (e) {
      console.log(e)
      return null
    }
  }
}
