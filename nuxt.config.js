const config = require('config')

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'LightOJ',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'LightOJ' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' }
    ],
    script: [
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-AMS_HTML' }
    ]
  },

  /*
  ** Global CSS
  */
  css: [
    'simplemde/dist/simplemde.min.css',
    'github-markdown-css',
    '~/assets/vendors/material-icons/material-icons.css',
    '~/assets/vendors/mono-social-icons/monosocialiconsfont.css',
    '~/assets/vendors/feather-icons/feather.css',
    '~/assets/scss/style.scss'
  ],
  /*
  ** Add axios globally
  */
  modules: [
    '@nuxtjs/axios',
    ['bootstrap-vue/nuxt', { css: false }]
  ],
  build: {
    vendor: [''],
    /*
    ** Run ESLINT on save
    */
    extend (config, ctx) {
      config.node = {
        console: true,
        fs: 'empty',
        net: 'empty',
        tls: 'empty'
      }
      /*
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
      */
    }
  },
  axios: {
    baseURL: config.nuxt.apiBaseUrl
  },
  router: {
    middleware: 'i18n'
  },
  watchers: {
    webpack: {
      poll: true
    }
  },
  plugins: [
    { src: '~plugins/i18n' },
    { src: '~plugins/vue-timeago', ssr: false },
    { src: '~plugins/nuxt-simplemde-plugin.js', ssr: false },
    '~/plugins/axios'
  ],
  loading: {
    color: '#99ccff',
    height: '10px'
  }
}
