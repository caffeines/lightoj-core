export default function ({ $axios, redirect }) {
  $axios.interceptors.response.use((response) => {
    return response
  }, function (error) {
    return Promise.reject(error)
  })
}
