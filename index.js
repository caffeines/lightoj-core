import server from './server'

const host = process.env.HOST || '0.0.0.0'
const port = process.env.PORT || 3000

// Listen the server
server.listen(port, host, function () {
  console.log('Server listening on ' + host + ':' + port) // eslint-disable-line no-console
})
